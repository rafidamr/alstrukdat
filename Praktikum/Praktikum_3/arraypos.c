// Muhammad Rafid Amrullah / 13515125

/* MODUL TABEL INTEGER DENGAN ELEMEN POSITIF */
/* Berisi definisi dan semua primitif pemrosesan tabel integer dengan elemen positif */
/* Penempatan elemen selalu rapat kiri */
/* Banyaknya elemen didefinisikan secara implisit, memori tabel statik */

#include "boolean.h"
#include "arraypos.h"
#include <stdio.h>
#include <math.h>

/* ********** KONSTRUKTOR ********** */
/* Konstruktor : create tabel kosong  */
void MakeEmpty (TabInt * T)
/* I.S. T sembarang */
/* F.S. Terbentuk tabel T kosong dengan kapasitas IdxMax-IdxMin+1 */
/* Proses: Inisialisasi semua elemen tabel T dengan ValUndef */
{	int i;
	for(i=IdxMin;i<=IdxMax;i++)
		(*T).TI[i] = ValUndef;
}
/* ********** SELEKTOR (TAMBAHAN) ********** */
/* *** Banyaknya elemen *** */
int NbElmt (TabInt T)
/* Mengirimkan banyaknya elemen efektif tabel */
/* Mengirimkan nol jika tabel kosong */
/* *** Daya tampung container *** */
{	int i;
	int ef = 0;
	for(i=IdxMin;i<=IdxMax;i++){
		if(T.TI[i] != 0)
			ef += 1;
	}
	return ef;
}
int MaxNbEl (TabInt T)
/* Mengirimkan maksimum elemen yang dapat ditampung oleh tabel */
/* *** Selektor INDEKS *** */
{
	return IdxMax;
}
IdxType GetFirstIdx (TabInt T)
/* Prekondisi : Tabel T tidak kosong */
/* Mengirimkan indeks elemen T pertama */
{
	return IdxMin;
}
IdxType GetLastIdx (TabInt T)
/* Prekondisi : Tabel T tidak kosong */
/* Mengirimkan indeks elemen T terakhir */
{	
	return NbElmt(T);
}
/* ********** Test Indeks yang valid ********** */
boolean IsIdxValid (TabInt T, IdxType i)
/* Mengirimkan true jika i adalah indeks yang valid utk ukuran tabel */
/* yaitu antara indeks yang terdefinisi utk container*/
{
	if((i<=IdxMax)&&(i>=IdxMin))
		return true;
	else
		return false;
}
boolean IsIdxEff (TabInt T, IdxType i)
/* Mengirimkan true jika i adalah indeks yang terdefinisi utk tabel */
/* yaitu antara FirstIdx(T)..LastIdx(T) */
{
	if((i>=GetFirstIdx(T))&&(i<=GetLastIdx(T)))
		return true;
	else
		return false;
}
/* ********** TEST KOSONG/PENUH ********** */
/* *** Test tabel kosong *** */
boolean IsEmpty (TabInt T)
/* Mengirimkan true jika tabel T kosong, mengirimkan false jika tidak */
{
	return NbElmt(T) <= 0;
}
/* *** Test tabel penuh *** */

boolean IsFull (TabInt T)
/* Mengirimkan true jika tabel T penuh, mengirimkan false jika tidak */
{
	return NbElmt(T) >= MaxNbEl(T);
}
/* ********** BACA dan TULIS dengan INPUT/OUTPUT device ********** */
/* *** Mendefinisikan isi tabel dari pembacaan *** */
void BacaIsi (TabInt * T)
/* I.S. T sembarang */
/* F.S. Tabel T terdefinisi */
/* Proses : membaca banyaknya elemen T dan mengisi nilainya */
/* 1. Baca banyaknya elemen diakhiri enter, misalnya N */
/*    Pembacaan diulangi sampai didapat N yang benar yaitu 0 <= N <= MaxNbEl(T) */
/*    Jika N tidak valid, tidak diberikan pesan kesalahan */
/* 2. Jika 0 < N <= MaxNbEl(T); Lakukan N kali: Baca elemen mulai dari indeks 
      IdxMin satu per satu diakhiri enter */
/*    Jika N = 0; hanya terbentuk T kosong */
{	int n,i;
	MakeEmpty(T);
	scanf("%d",&n);
	while((n<0)||(n>IdxMax))
		scanf("%d",&n);
	
	for(i=IdxMin;i<=n;i++)
		scanf("%d",&(*T).TI[i]);
}
void TulisIsiTab (TabInt T)
/* Proses : Menuliskan isi tabel dengan traversal, tabel ditulis di antara kurung siku; 
   antara dua elemen dipisahkan dengan separator "koma", tanpa tambahan karakter di depan,
   di tengah, atau di belakang, termasuk spasi dan enter */
/* I.S. T boleh kosong */
/* F.S. Jika T tidak kosong: [e1,e2,...,en] */
/* Contoh : jika ada tiga elemen bernilai 1, 20, 30 akan dicetak: [1,20,30] */
/* Jika tabel kosong : menulis [] */
{	int i = 1;
	printf("[");
	if(!IsEmpty(T)){
	for(i=IdxMin;i<=GetLastIdx(T);i++){
			if(i!=GetLastIdx(T))
				printf("%d,",T.TI[i]);
			else
				printf("%d",T.TI[i]);
		}
	}
	printf("]");
}
/* ********** OPERATOR ARITMATIKA ********** */
/* *** Aritmatika tabel : Penjumlahan, pengurangan, perkalian, ... *** */
TabInt PlusMinusTab (TabInt T1, TabInt T2, boolean plus)
/* Prekondisi : T1 dan T2 berukuran sama dan tidak kosong */
/* Jika plus = true, mengirimkan  T1+T2, yaitu setiap elemen T1 dan T2 pada indeks yang sama dijumlahkan */
/* Jika plus = false, mengirimkan T1-T2, yaitu setiap elemen T1 dikurangi elemen T2 pada indeks yang sama */
{	int i,j,k;
	TabInt T;
	j = IdxMin;
	k = IdxMin;
	MakeEmpty(&T);
	for(i=IdxMin;i<=GetLastIdx(T1);i++,j++,k++){
		if(plus == true)
			T.TI[i] = T1.TI[j] + T2.TI[k];
		else
			T.TI[i] = T1.TI[j] - T2.TI[k];
	}
	return T;
}
/* ********** OPERATOR RELASIONAL ********** */
/* *** Operasi pembandingan tabel : < =, > *** */
boolean IsEQ (TabInt T1, TabInt T2)
/* Mengirimkan true jika T1 sama dengan T2 yaitu jika ukuran T1 = T2 dan semua elemennya sama */
{	int i;
	boolean b = true;
	
	if(NbElmt(T1) == NbElmt(T2)){
		if(IsEmpty(T1)){
			return b;
		}else{
			for(i=IdxMin;i<=GetLastIdx(T1);i++){
				if(T1.TI[i] != T2.TI[i])
					b = false;
			}
			return b;
		}
	}else
		return false;
}
/* ********** SEARCHING ********** */
/* ***  Perhatian : Tabel boleh kosong!! *** */
IdxType Search1 (TabInt T, ElType X)
/* Search apakah ada elemen tabel T yang bernilai X */
/* Jika ada, menghasilkan indeks i terkecil, dengan elemen ke-i = X */
/* Jika tidak ada, mengirimkan IdxUndef */
/* Menghasilkan indeks tak terdefinisi (IdxUndef) jika tabel T kosong */
/* Skema Searching yang digunakan bebas */
{	int o,h = 1;
	IdxType i;
	if(IsEmpty(T))
		return IdxUndef;
	else{
		while(h<=GetLastIdx(T)){
			if(T.TI[h] == X){
				i = h;
				h = GetLastIdx(T)+1;
				o = 0;}
			h++;
		}
	}
	if(o==0)
		return i;
	else
		return IdxUndef;
}
boolean SearchB (TabInt T, ElType X)
/* Search apakah ada elemen tabel T yang bernilai X */
/* Jika ada, menghasilkan true, jika tidak ada menghasilkan false */
/* Skema searching yang digunakan bebas */
{	int o,h = 1;
	IdxType i;
	if(IsEmpty(T))
		return false;
	else{
		while(h<=GetLastIdx(T)){
			if(T.TI[h] == X){
				i = h;
				h = GetLastIdx(T)+1;
				o = 0;}
			h++;
		}
	}
	if(o==0)
		return true;
	else
		return false;
}
/* ********** NILAI EKSTREM ********** */
void MaxMin (TabInt T, ElType * Max, ElType * Min)
/* I.S. Tabel T tidak kosong */
/* F.S. Max berisi nilai maksimum T;
        Min berisi nilai minimum T */
{	int i;
	int max = -9999;
	int min = 9999;
	for(i=IdxMin;i<=GetLastIdx(T);i++){
		if(T.TI[i]>max)
			max = T.TI[i];
		if(T.TI[i]<min)
			min = T.TI[i];
	}
	(*Max) = max;
	(*Min) = min;
}
/* ********** OPERASI LAIN ********** */
ElType SumTab (TabInt T)
/* Menghasilkan hasil penjumlahan semua elemen T */
/* Jika T kosong menghasilkan 0 */
{	int i,j = 0;
	for(i=IdxMin;i<=GetLastIdx(T);i++)
		j += T.TI[i];
	if(IsEmpty(T))
		return 0;
	else
		return j;
}
int CountX (TabInt T, ElType X)
/* Menghasilkan berapa banyak kemunculan X di T */
/* Jika T kosong menghasilkan 0 */
{	int i,j = 0;
	for(i=IdxMin;i<=GetLastIdx(T);i++){
		if(T.TI[i] == X)
			j += 1;
	}
	return j;
}
boolean IsAllGenap (TabInt T)
/* Menghailkan true jika semua elemen T genap */
{	int i,j = 0;
	for(i=IdxMin;i<=GetLastIdx(T);i++){
		if((T.TI[i]%2)==0)
			j += 1;
	}
	if(j==GetLastIdx(T))
		return true;
	else
		return false;
}
/* ********** SORTING ********** */
void Sort (TabInt * T, boolean asc)
/* I.S. T boleh kosong */
/* F.S. Jika asc = true, T terurut membesar */
/*      Jika asc = false, T terurut mengecil */
/* Proses : Mengurutkan T dengan salah satu algoritma sorting,
   algoritma bebas */
{
	int i,j,c,p;
	int max = -9999;
	int min = 9999;
	
	for(i=IdxMin;i<=GetLastIdx(*T)-i+1;i++){
		for(j=IdxMin;j<=GetLastIdx(*T)-i+1;j++){
			if(asc == true){
				if((*T).TI[j]>max){
					max = (*T).TI[j];
					p = j;}
			}else{
				if((*T).TI[j]<min){
					min = (*T).TI[j];
					p = j;}
			}
		}
		c = (*T).TI[GetLastIdx(*T)-i+1];
		(*T).TI[GetLastIdx(*T)-i+1] = (*T).TI[p];
		(*T).TI[p] = c;
	}
}

/* ********** MENAMBAH DAN MENGHAPUS ELEMEN DI AKHIR ********** */
/* *** Menambahkan elemen terakhir *** */
void AddAsLastEl (TabInt * T, ElType X)
/* Proses: Menambahkan X sebagai elemen terakhir tabel */
/* I.S. Tabel T boleh kosong, tetapi tidak penuh */
/* F.S. X adalah elemen terakhir T yang baru */
{	int p;

	if(!IsFull(*T)){
		if(IsEmpty(*T)){
			(*T).TI[IdxMin] = X;
		}else{
			p = GetLastIdx(*T)+1;
			(*T).TI[p] = X;
		}
	}
}
/* ********** MENGHAPUS ELEMEN ********** */
void DelLastEl (TabInt * T, ElType * X)
/* Proses : Menghapus elemen terakhir tabel */
/* I.S. Tabel tidak kosong */
/* F.S. X adalah nilai elemen terakhir T sebelum penghapusan, */
/*      Banyaknya elemen tabel berkurang satu */
/*      Tabel T mungkin menjadi kosong */
{
		*X = (*T).TI[GetLastIdx(*T)];
		(*T).TI[GetLastIdx(*T)] = 0;
}