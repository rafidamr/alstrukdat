//Nama: Muhammad Rafid Amrullah
//NIM : 13515125

/* Definisi type boolean */

#ifndef _BOOLEAN_h
#define _BOOLEAN_h

#define boolean unsigned char
#define true 1
#define false 0

#endif