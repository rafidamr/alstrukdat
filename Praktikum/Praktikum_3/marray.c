//NIM					: 13515125
//Nama					: Muhammad Rafid Amrullah
//Tanggal				: 08 / 09 / 2016
//Topik praktikum		: ADT array
//Deskripsi				: ADT Array

#include <stdio.h>
#include "array.h"
#include "boolean.h"

/* MODUL TABEL INTEGER */
/* Berisi definisi dan semua primitif pemrosesan tabel integer */
/* Penempatan elemen selalu rapat kiri */
/* Versi I : dengan banyaknya elemen didefinisikan secara eksplisit, 
   memori tabel statik */


int main(){
	int sum,i,X;
	TabInt T;
	
	sum = 0;
	
	BacaIsi(&T);
	scanf("%d",&X);
	
	TulisIsiTab(T);
	printf("\n");
	
	for(i=1;i<=T.Neff;i++){
		if(T.TI[i] == X)
			sum += 1;
	}
	printf("%d\n",sum);
	
	if(SearchB (T,X) == false)
		printf("%d tidak ada\n",X);
	else
		printf("%d\n",Search2 (T,X));
	
	if(ValMax (T) == X)
		printf("maksimum\n");
	if(ValMin(T) == X)
		printf("minimum\n");
	InsSortAsc(&T);
	if(T.Neff % 2 == 0){
		if(X == T.TI[T.Neff/2])
			printf("median\n");
	}else{
		if(X == T.TI[(T.Neff+1)/2])
			printf("median\n");
	}
	
	return 0;
}
